![RoguelikeDev does the Complete Roguelike Tutorial; v6.0; 2022.6-8](https://i.imgur.com/FzwmyyE.png)

# Roguelike Tutorial Project 2022

This is my project made to follow
/r/roguelikedev's [annual tutorial code-along event.](https://www.reddit.com/r/roguelikedev/comments/vhfsda/roguelikedev_does_the_complete_roguelike_tutorial/)
While the [official tutorial](https://rogueliketutorials.com/tutorials/tcod/v2/) is made with Python, I will be using
Java instead, and adapt the code from the tutorial as best as I can. Notable libraries I am using include:

- [AsciiPanel](https://github.com/trystan/AsciiPanel)

## Repo Structure

I will tag commits with the part of the tutorial that is completed with them, e.g., the commit tagged with "part01" will
show the state of the project at the end of [part 1.](https://rogueliketutorials.com/tutorials/tcod/v2/part-1/) This
allows anyone who wants to follow along to see how the project changes as the tutorial progresses.