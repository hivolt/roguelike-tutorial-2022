// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import asciiPanel.AsciiPanel;
import hivolt.rltutorial.actions.Action;

import javax.swing.*;
import java.awt.*;

public class App extends JFrame {
	private static final int SCREEN_WIDTH = 80;
	private static final int SCREEN_HEIGHT = 50;
	private static final String WINDOW_TITLE = "RL Tutorial '22";

	private static final int MAP_WIDTH = 80;
	private static final int MAP_HEIGHT = 45;
	private static final int MAX_ROOM_SIZE = 10;
	private static final int MIN_ROOM_SIZE = 6;
	private static final int MAX_ROOMS = 30;
	private static final int MAX_ROOM_MONSTERS = 2;

	private final AsciiPanel panel;
	private final Engine engine;

	public App() {
		super(WINDOW_TITLE);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		panel = new AsciiPanel(SCREEN_WIDTH, SCREEN_HEIGHT);
		add(panel);
		pack();

		addKeyListener(new EventHandler(this));

		var player = new Entity(EntityRepo.PLAYER);

		var map = Generator.generate(MAX_ROOMS,
			MIN_ROOM_SIZE,
			MAX_ROOM_SIZE,
			MAP_WIDTH,
			MAP_HEIGHT,
			MAX_ROOM_MONSTERS,
			player);
		map.computeFov(player.getX(), player.getY(), 8);

		engine = new Engine(player, map);

		render();
	}

	public static void main(String[] args) {
		var app = new App();
		app.setVisible(true);
	}

	public void render() {
		panel.clear();
		engine.render(panel);
		repaint();
	}

	public void execute(Action action) {
		engine.execute(action);
		render();
	}
}
