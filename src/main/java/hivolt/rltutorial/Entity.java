// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.awt.*;

@RequiredArgsConstructor public class Entity {
	@Getter @Setter private int x;
	@Getter @Setter private int y;
	@Getter private final char symbol;
	@Getter private final Color color;
	@Getter private final String name;
	@Getter private final boolean solid;

	public Entity(Entity other) {
		symbol = other.symbol;
		color = other.color;
		name = other.name;
		solid = other.solid;
	}

	public void move(int dx, int dy) {
		x += dx;
		y += dy;
	}

	public Entity spawn(GameMap map, int x, int y) {
		Entity clone = new Entity(this);
		clone.x = x;
		clone.y = y;
		map.addEntity(clone);
		return clone;
	}
}
