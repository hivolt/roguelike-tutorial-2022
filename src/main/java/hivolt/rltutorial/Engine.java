// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import asciiPanel.AsciiPanel;
import hivolt.rltutorial.actions.Action;
import lombok.Getter;

public class Engine {
	private final Entity player;
	@Getter private final GameMap map;

	public Engine(Entity player, GameMap map) {
		this.player = player;
		this.map = map;
	}

	public void render(AsciiPanel panel) {
		map.render(panel);
	}

	public void execute(Action action) {
		if (action == null) {
			return;
		}
		action.perform(this, player);
		handleEnemyTurns();
		updateFov();
	}

	public void updateFov() {
		map.clearFov();
		map.computeFov(player.getX(), player.getY(), 8);
	}

	public void handleEnemyTurns() {
		for (Entity entity : map.getEntities()) {
			if (entity != player) {
				System.out.printf("The %s bides its time...\n", entity.getName());
			}
		}
	}
}
