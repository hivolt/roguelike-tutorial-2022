// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import java.awt.*;

public record Graphic(char symbol, Color fg, Color bg) {
}
