// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import asciiPanel.AsciiPanel;

import java.awt.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class GameMap {
	private static final Graphic SHROUD = new Graphic(' ', Color.WHITE, Color.BLACK);

	private final int width;
	private final int height;
	private final Tile[][] tiles;
	private final boolean[][] visible;
	private final boolean[][] explored;
	private final Collection<Entity> entities = new HashSet<>();

	public GameMap(int width, int height) {
		this.width = width;
		this.height = height;

		tiles = new Tile[width][height];
		visible = new boolean[width][height];
		explored = new boolean[width][height];

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				tiles[x][y] = Tile.WALL;
				visible[x][y] = false;
				explored[x][y] = false;
			}
		}
	}

	private Graphic getGraphic(int x, int y) {
		if (!isInBounds(x, y)) {
			return SHROUD;
		}

		Tile tile = tiles[x][y];
		return visible[x][y] ? tile.getLightGraphic() : explored[x][y] ? tile.getDarkGraphic() : SHROUD;
	}

	private void lightTile(int x, int y) {
		if (isInBounds(x, y)) {
			visible[x][y] = true;
			explored[x][y] = true;
		}
	}

	private void computeLos(int x1, int y1, int x2, int y2) {
		// Adapted from:
		// http://roguebasin.com/index.php/LOS_using_strict_definition
		int dx = x2 - x1;
		int dy = y2 - y1;
		int mx = x1 < x2 ? 1 : -1;
		int my = y1 < y2 ? 1 : -1;

		double dist = Math.sqrt(dx * dx + dy * dy);
		int x = x1;
		int y = y1;

		while (x != x2 || y != y2) {
			lightTile(x, y);
			if (!tiles[x][y].isTransparent()) {
				return;
			}
			double px = Math.abs(dy * (x - x1 + mx) - dx * (y - y1)) / dist;
			double py = Math.abs(dy * (x - x1) - dx * (y - y1 + my)) / dist;
			if (px < 0.5d) {
				x += mx;
			} else if (py < 0.5d) {
				y += my;
			} else {
				x += mx;
				y += my;
			}
		}
		lightTile(x2, y2);
	}

	public boolean isInBounds(int x, int y) {
		return x >= 0 && x < width && y >= 0 && y < height;
	}

	public boolean isWalkable(int x, int y) {
		return isInBounds(x, y) && tiles[x][y].isWalkable();
	}

	public Color getBg(int x, int y) {
		return getGraphic(x, y).bg();
	}

	public void render(AsciiPanel panel) {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				var graphic = getGraphic(x, y);
				panel.write(graphic.symbol(), x, y, graphic.fg(), graphic.bg());
			}
		}
		for (Entity entity : entities) {
			if (isInFov(entity.getX(), entity.getY())) {
				panel.write(entity.getSymbol(),
					entity.getX(),
					entity.getY(),
					entity.getColor(),
					getBg(entity.getX(), entity.getY()));
			}
		}
	}

	public void putFloor(int x, int y) {
		if (isInBounds(x, y)) {
			tiles[x][y] = Tile.FLOOR;
		}
	}

	public void clearFov() {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				visible[x][y] = false;
			}
		}
	}

	public void computeFov(int startX, int startY, int radius) {
		for (int i = -radius; i <= radius; i++) {
			for (int j = -radius; j <= radius; j++) {
				if (i * i + j * j < radius * radius) {
					computeLos(startX, startY, startX + i, startY + j);
				}
			}
		}
	}

	public boolean isInFov(int x, int y) {
		return isInBounds(x, y) && visible[x][y];
	}

	public void addEntity(Entity entity) {
		entities.add(entity);
	}

	public Optional<Entity> getBlockingEntity(int x, int y) {
		return entities.stream().filter(e -> e.getX() == x && e.getY() == y && e.isSolid()).findFirst();
	}

	public Iterable<Entity> getEntities() {
		return entities;
	}
}
