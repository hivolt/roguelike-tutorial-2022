// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial.actions;

import hivolt.rltutorial.Engine;
import hivolt.rltutorial.Entity;

public record EscapeAction() implements Action {

	@Override public void perform(Engine engine, Entity entity) {
		System.exit(0);
	}
}
