// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial.actions;

import hivolt.rltutorial.Engine;
import hivolt.rltutorial.Entity;

public class MeleeAction extends DirectedAction {
	public MeleeAction(int dx, int dy) {
		super(dx, dy);
	}

	@Override public void perform(Engine engine, Entity entity) {
		int destX = entity.getX() + dx;
		int destY = entity.getY() + dy;

		var target = engine.getMap().getBlockingEntity(destX, destY);
		if (target.isEmpty()) {
			return;
		}

		System.out.printf("You kick the %s and annoy it.\n", target.get().getName());
	}
}
