// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial.actions;

import hivolt.rltutorial.Engine;
import hivolt.rltutorial.Entity;

public class BumpAction extends DirectedAction {

	public BumpAction(int dx, int dy) {
		super(dx, dy);
	}

	@Override public void perform(Engine engine, Entity entity) {
		int destX = entity.getX() + dx;
		int destY = entity.getY() + dy;

		if (engine.getMap().getBlockingEntity(destX, destY).isPresent()) {
			new MeleeAction(dx, dy).perform(engine, entity);
		} else {
			new MovementAction(dx, dy).perform(engine, entity);
		}
	}
}
