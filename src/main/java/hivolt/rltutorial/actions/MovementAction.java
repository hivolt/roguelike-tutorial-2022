// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial.actions;

import hivolt.rltutorial.Engine;
import hivolt.rltutorial.Entity;

public class MovementAction extends DirectedAction {

	public MovementAction(int dx, int dy) {
		super(dx, dy);
	}

	@Override public void perform(Engine engine, Entity entity) {
		var map = engine.getMap();
		int destX = entity.getX() + dx;
		int destY = entity.getY() + dy;

		if (!map.isWalkable(destX, destY)) {
			return;
		} else if (map.getBlockingEntity(destX, destY).isPresent()) {
			return;
		}
		entity.move(dx, dy);
	}
}
