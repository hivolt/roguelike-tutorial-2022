// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial.actions;

import hivolt.rltutorial.Engine;
import hivolt.rltutorial.Entity;

public interface Action {

	void perform(Engine engine, Entity entity);
}
