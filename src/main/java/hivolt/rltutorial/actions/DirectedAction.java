// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial.actions;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor public abstract class DirectedAction implements Action {
	protected final int dx;
	protected final int dy;
}
