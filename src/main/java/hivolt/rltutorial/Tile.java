// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.awt.*;

@RequiredArgsConstructor public enum Tile {
	FLOOR(true,
		true,
		new Graphic(' ', Color.WHITE, new Color(50, 50, 150)),
		new Graphic(' ', Color.WHITE, new Color(200, 180, 50))),
	WALL(false,
		false,
		new Graphic(' ', Color.WHITE, new Color(0, 0, 100)),
		new Graphic(' ', Color.WHITE, new Color(130, 110, 50))),
	;
	@Getter private final boolean walkable;
	@Getter private final boolean transparent;
	@Getter private final Graphic darkGraphic;
	@Getter private final Graphic lightGraphic;
}
