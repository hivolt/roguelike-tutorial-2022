// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

public record Point(int x, int y) {
}
