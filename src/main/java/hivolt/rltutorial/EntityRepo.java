// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import java.awt.*;

public final class EntityRepo {
	public static final Entity PLAYER = new Entity('@', Color.WHITE, "Player", true);
	public static final Entity ORC = new Entity('o', new Color(63, 127, 63), "Orc", true);
	public static final Entity TROLL = new Entity('T', new Color(0, 127, 0), "Troll", true);
}
