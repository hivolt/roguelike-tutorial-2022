// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {
	private static Random random = new Random();

	private static void digTunnel(GameMap map, Point start, Point end) {
		var corner = random.nextFloat() < 0.5 ? new Point(end.x(), start.y()) : new Point(start.x(), end.y());

		for (var point : Lines.bresenham(start, corner)) {
			map.putFloor(point.x(), point.y());
		}
		for (var point : Lines.bresenham(corner, end)) {
			map.putFloor(point.x(), point.y());
		}
	}

	private static void digRoom(GameMap map, RectangularRoom room) {
		for (Point point : room.getInner()) {
			map.putFloor(point.x(), point.y());
		}
	}

	private static void placeEntities(GameMap map, RectangularRoom room, int maxMonsters) {
		var p1 = room.getTopLeft();
		var p2 = room.getBottomRight();

		int monsterCount = random.nextInt(maxMonsters + 1);

		for (int i = 0; i < monsterCount; i++) {
			int x = random.nextInt(p1.x() + 1, p2.x());
			int y = random.nextInt(p1.y() + 1, p2.y());

			if (map.getBlockingEntity(x, y).isEmpty()) {
				Entity entity = random.nextFloat() < 0.8 ? EntityRepo.ORC : EntityRepo.TROLL;
				entity.spawn(map, x, y);
			}
		}
	}

	public static GameMap generate(int maxRooms,
		int minRoomSize,
		int maxRoomSize,
		int mapWidth,
		int mapHeight,
		int maxMonstersPerRoom,
		Entity player) {
		var dungeon = new GameMap(mapWidth, mapHeight);

		List<RectangularRoom> rooms = new ArrayList<>();

		for (int r = 0; r < maxRooms; r++) {
			int roomWidth = random.nextInt(minRoomSize, maxRoomSize + 1);
			int roomHeight = random.nextInt(minRoomSize, maxRoomSize + 1);
			int roomX = random.nextInt(0, mapWidth - roomWidth);
			int roomY = random.nextInt(0, mapHeight - roomHeight);

			var newRoom = new RectangularRoom(roomX, roomY, roomWidth, roomHeight);
			if (rooms.stream().anyMatch(newRoom::intersects)) {
				continue;
			}
			digRoom(dungeon, newRoom);
			if (rooms.size() == 0) {
				var center = newRoom.getCenter();
				player.setX(center.x());
				player.setY(center.y());
			} else {
				var prevRoom = rooms.get(rooms.size() - 1);
				digTunnel(dungeon, prevRoom.getCenter(), newRoom.getCenter());
			}
			placeEntities(dungeon, newRoom, maxMonstersPerRoom);
			rooms.add(newRoom);
		}

		dungeon.addEntity(player);
		return dungeon;
	}
}
