// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import java.util.ArrayList;
import java.util.List;

public final class Lines {
	public static Iterable<Point> bresenham(Point start, Point end) {
		// Adapted from:
		// https://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#Java
		List<Point> points = new ArrayList<>();

		int x1 = Math.min(start.x(), end.x());
		int x2 = Math.max(start.x(), end.x());
		int y1 = Math.min(start.y(), end.y());
		int y2 = Math.max(start.y(), end.y());

		int d = 0;
		int dx = Math.abs(x2 - x1);
		int dy = Math.abs(y2 - y1);

		int x = x1;
		int y = y1;

		if (dx > dy) {
			while (x <= x2) {
				points.add(new Point(x, y));
				x++;
				d += dy * 2;
				if (d > dx) {
					y++;
					d -= dx * 2;
				}
			}
		} else {
			while (y <= y2) {
				points.add(new Point(x, y));
				y++;
				d += dx * 2;
				if (d > dy) {
					x++;
					d -= dy * 2;
				}
			}
		}
		return points;
	}
}
