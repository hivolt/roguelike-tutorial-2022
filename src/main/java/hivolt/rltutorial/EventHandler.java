// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import hivolt.rltutorial.actions.Action;
import hivolt.rltutorial.actions.BumpAction;
import hivolt.rltutorial.actions.EscapeAction;
import lombok.AllArgsConstructor;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

@AllArgsConstructor public class EventHandler implements KeyListener {
	private App owner;

	@Override public void keyPressed(KeyEvent keyEvent) {
		Action action = switch (keyEvent.getKeyCode()) {
			case KeyEvent.VK_UP -> new BumpAction(0, -1);
			case KeyEvent.VK_DOWN -> new BumpAction(0, 1);
			case KeyEvent.VK_LEFT -> new BumpAction(-1, 0);
			case KeyEvent.VK_RIGHT -> new BumpAction(1, 0);
			case KeyEvent.VK_ESCAPE -> new EscapeAction();
			default -> null;
		};
		owner.execute(action);
	}

	@Override public void keyReleased(KeyEvent keyEvent) {
	}

	@Override public void keyTyped(KeyEvent keyEvent) {
	}
}
