// SPDX-License-Identifier: MPL-2.0
package hivolt.rltutorial;

import lombok.Getter;

import java.util.stream.IntStream;

public class RectangularRoom {
	@Getter private final Point topLeft;
	@Getter private final Point bottomRight;

	RectangularRoom(int x, int y, int w, int h) {
		topLeft = new Point(x, y);
		bottomRight = new Point(x + w, y + h);
	}

	public Point getCenter() {
		int centerX = (topLeft.x() + bottomRight.x()) / 2;
		int centerY = (topLeft.y() + bottomRight.y()) / 2;
		return new Point(centerX, centerY);
	}

	public Iterable<Point> getInner() {
		var xs = IntStream.rangeClosed(topLeft.x() + 1, bottomRight.x() - 1).boxed();
		return xs.flatMap(x -> {
			var ys = IntStream.rangeClosed(topLeft.y() + 1, bottomRight.y() - 1).boxed();
			return ys.map(y -> new Point(x, y));
		}).toList();
	}

	public boolean intersects(RectangularRoom other) {
		return topLeft.x() <= other.bottomRight.x()
			&& bottomRight.x() >= other.topLeft.x()
			&& topLeft.y() <= other.bottomRight.y()
			&& bottomRight.y() >= other.topLeft.y();
	}
}
